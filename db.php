<?php

class db
{
	protected $db;

    private static $instance;

# -------
    public static function getInstance($CFG)
    {
    	if (!isset(self::$instance)) {
        	$c = __CLASS__;
        	self::$instance = new $c($CFG);
      	}
		return self::$instance;
    }
# ------- 
	public function __construct($CFG) 
	{
	   	$this->db = new PDO('mysql:host=' . $CFG['hostname'] . '; dbname=' . $CFG['database'], $CFG['username'], $CFG['password']);
		$this->db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
# ------- 
	public function check($surName)
	{	
		$sth = $this->db->prepare("SELECT * FROM people WHERE Surname LIKE :surName"); 
		$sth->bindParam(':surName', $surName, PDO::PARAM_STR);
		$sth->execute();	

    	$output = $sth->fetch(PDO::FETCH_ASSOC);	
		$sth->closeCursor();
		return $output;
	}			
#--------
	public function getBullhornSkillIds($fromIds) 
	{
		$sth = $this->db->query('SELECT fa.farea_bh_value '.
			'FROM category_functional_area fa '.
		'WHERE fa.farea_id IN ('. $fromIds .') '.
			'AND fa.farea_bh_value IS NOT NULL '.
			'AND fa.farea_bh_value <> "" ');
			
		$data = $sth->fetchAll(PDO::FETCH_COLUMN);
		$res = array();
		if (is_array($data) && count($data)) {
			foreach ($data as $value) {
				array_push($res, intval($value));
			}
		}
		return $res;
	}	
	
} ?>
