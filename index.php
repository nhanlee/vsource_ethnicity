<?php
 	//GET config host db - Change to RDS
    define('CONFPATH', dirname(__FILE__));
    $conf = parse_ini_file(CONFPATH . '/config.ini', true);
    if(!$conf){
            mail("vsource.dev@vsource.io", "vSource CV2BH2.0 (Can not read config file)", "For more details, check config.ini",
        "From: vSource <vsource@support.glandoresystems.com>\n" .
        "Reply-To: vsource@support.glandoresystems.com\n" .
        "X-Mailer: GSMail/1.0.3");
            return false;
    }

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json; charset=utf-8');

	define('DIRPATH', dirname(__FILE__).'/');
	define('LOGPATH', dirname(__FILE__).'/logs/');
	if (!is_dir(LOGPATH)){
		mkdir(LOGPATH, 0777, true);
	}
# -------
	function logMessage($message)
	{
		error_log(date('Y-m-d H:i:s') . "\t\t" . $message . "\n", 3, DIRPATH . 'logs/' . date('Ymd') . '.log');
	}

# -------	
	$CFG = array(
		'db' => array(
			'hostname'      =>  $conf['db']['host'],
			'database'      =>  $conf['db']['database'],
			'username'      =>  $conf['db']['username'],
			'password'      =>  $conf['db']['password']
	  	)
	);
# -------	
	require_once(DIRPATH . 'db.php');	
	$db = new db($CFG['db']);	
	db::getInstance($CFG['db']);
	if(isset($_GET['surname'])){
		$surName = $_GET['surname'];
		$surName = ucwords($surName);
		
		//Log
		logMessage('Check Surname: ' . $surName);

		$data = $db->check($surName);
		if($data){
			echo json_encode($data);
		} else {
			echo json_encode(array('Surname' => $surName, 'id' => null));
		}
	} else {
		echo json_encode(array('Surname' => $surName, 'id' => null));
	}
?>
