<?php		
		$sql = "
		CREATE TABLE IF NOT EXISTS `people` (
		  `id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		  `Surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
		  `White` DECIMAL(5,2) NOT NULL,
		  `Black` DECIMAL(5,2) NOT NULL,
		  `Asian` DECIMAL(5,2) NOT NULL,
		  `Hispanic` DECIMAL(5,2) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
    	$this->execute($sql);